import http = require('http')
import fs = require('fs')
import { parse } from "url";

export type Response<T> = Promise< T | { message: string }>

export type conf = { protocol: 'unix:', socketPath: string } | { protocol: 'http:', hostname: string, port: string }

export type DockerRequestOptions = {
  path: string
  params?: object
  // method?: string
}
export type DockerRequestParam = string|DockerRequestOptions
export const DefaultDockerRequestOptions = {
  path: '',
  params: {},
}
export interface DockerRequest {
  <T=any>(path:DockerRequestParam,origin?:false):Promise<T>
  (path:DockerRequestParam,origin:true):Promise<http.IncomingMessage>
}

export class DockerAPI {
  /**
   * 版本号, 示例: `/v1.37` . (需要带上`/`)
   */
  version: string
  conf: conf

  static get_server_uri_from_env = ()=>{
    if(process.env.DOCKER_HOST){
      return process.env.DOCKER_HOST
    }else if(fs.existsSync('/var/run/docker.sock')){
      return 'unix:///var/run/docker.sock'
    }else{
      throw new Error("can't get docker server uri from env, and /var/run/docker.sock is not exists")
    }
  }
  
  constructor(
    /**例子: `unix:///var/run/docker.sock` | `tcp://127.0.0.1:2375` */
    docker_server_uri:string = DockerAPI.get_server_uri_from_env()
  ){
    let { protocol, hostname, port, pathname } = parse(docker_server_uri)
    protocol = protocol.replace(/\:$/,'')
    switch(protocol){
      case 'unix':
        this.conf = { protocol: 'unix:', socketPath: pathname, }
        break
      case 'tcp':
        this.conf = { protocol: "http:", hostname, port, }
        break;
    }
  }

  request:DockerRequest = (docker_opts:DockerRequestParam,origin=false):any=>new Promise((rl,rj)=>{

    function err_handler(err:Error){ req.abort(); rj(err) }
    
    let req_opts:http.RequestOptions 
    if(typeof docker_opts === 'string'){
      req_opts = { 
        ...this.conf,
        path: docker_opts,
      }
    }else{
      let query = ''
      docker_opts = {
        ...DefaultDockerRequestOptions,
        ...docker_opts,
      }
      for(let name in docker_opts.params){
        // @ts-ignore
        let val = JSON.stringify(docker_opts.params[name])
        query += `&${name}=`+decodeURIComponent(val)
      }
      query = query.slice(1)
      req_opts = {
        ...this.conf,
        path: docker_opts.path+'?'+query,
      }
    }
    
    req_opts.path = [ this.version, req_opts.path ].join('')
    let req = http.get(req_opts, res=>{
      if(origin){ return rl(res) }
      let str = ''
      res
        .on('data',chunk=>str+=chunk)
        .on('end',()=>{
          let response:{message:string}
          try {
            response = JSON.parse(str)
          }catch(err){
            rj(err)
          }
          if(response.message){
            response.message = res.statusCode +' '+ response.message
            rj(response)
          }else{
            rl(response)
          }
        })
        .once('error',err_handler)
    })
    req.once('error',err_handler)

  })

}