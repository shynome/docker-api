import { DockerAPI as Base, Response } from "../Docker";
import { DockerListItem } from "./list";
import { DockerStats } from "./stats";

export class DockerAPI extends Base {

  version = '/v1.37'
  
  async stats(id:string):Response<DockerStats>{
    return this.request(`/containers/${id}/stats?stream=0`)
  }

  async ps(params:{ filter?: object }={}):Response<DockerListItem[]>{
    return this.request({
      path: '/containers/json',
      params: params,
    })
  }

}