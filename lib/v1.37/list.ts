export interface DockerListItem {
  id: string
  Names: string[]
  Image: string
  ImageID: string
  Command: string
  /**时间戳 */
  Created: number
  Ports: {
    IP: string,
    PrivatePort: number,
    PublishPort: number,
    Type: 'tcp'|'udp'
  }[]
  Labels: { [key:string]: string }
  State: 'running'
  Status: string,
  HostConfig: {
    NetworkMode: 'default'
  }
  NetworkSettings: {
    Networks: {
      bridge: {
        IPAMConfig: null
        Links: null
        Aliases: null
        NetworkID: string
        EndpointID: string
        Gateway: string
        IPAddress: string
        IPPrefixLen: number
        IPv6Gateway: string
        GlobalIPv6Address: string
        GlobalIPv6PrefixLen: 0
        MacAddress: string
        DriverOpts: null
      }
    }
  }
	Mounts: {
    Type: 'volume'
    Name: string
    Source: string
    Destination: string
    Driver: 'local'
    Mode: string
    RW: boolean
    Propagation: string
	}[]

}
