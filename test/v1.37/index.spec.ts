import { deepStrictEqual } from "assert";

import { DockerAPI } from "../../lib/v1.37";

describe('v1.37',()=>{
  const docker = new DockerAPI()
  it('stats',async ()=>{
    await docker.stats('4dcf006b0405')
  })
  it('ps',async ()=>{
    await docker.ps({
      filter: {"name":{"influxdb":true} }
    })
  })
  
})
